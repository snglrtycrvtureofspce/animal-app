import { Component, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { NgToastService } from 'ng-angular-popup';

interface Location {
  id: string;
  createdDate: string;
  modificationDate: string;
  name: string;
  description: string;
  latitude: number;
  longitude: number;
}

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {
  isAdmin: boolean = false;
  locations: Location[] = [];
  locationsWithoutFilter: Location[] = [];
  newLocation: Partial<Location> = {};
  editingLocation: Location | null = null;
  loading: boolean = false;
  showCreateModal: boolean = false;

  NameFilter: string = '';
  DescriptionFilter: string = '';
  LatitudeFilter: string = '';
  LongitudeFilter: string = '';
  sortColumn: keyof Location = 'name';
  sortAsc: boolean = true;

  constructor(private http: HttpClient, private toast: NgToastService, private auth: AuthService) { }

  ngOnInit(): void {
    this.isAdmin = this.auth.isAdmin();
    this.getLocations();
  }

  showScrollButton: boolean = false;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (window.pageYOffset > 300) {
      this.showScrollButton = true;
    } else {
      this.showScrollButton = false;
    }
  }

  scrollToTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  getLocations() {
    this.loading = true;
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: Location[] }>('https://api.snglrtycrvtureofspce.me/Location')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка местоположений:', error);
          return EMPTY;
        }),
        finalize(() => this.loading = false)
      )
      .subscribe(response => {
        this.locations = response.elements;
        this.locationsWithoutFilter = response.elements;
      });
  }

  createLocation() {
    if (!this.newLocation.name || !this.newLocation.description || !this.newLocation.latitude || !this.newLocation.longitude) {
      return;
    }
    this.loading = true;
    this.http.post('https://api.snglrtycrvtureofspce.me/Location', this.newLocation)
      .pipe(
        catchError(error => {
          console.error('Ошибка при создании местоположения:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Местоположение успешно создано" });
          this.getLocations();
          this.newLocation = {};
          this.showCreateModal = false;
        })
      )
      .subscribe();
  }

  updateLocation(id: string, location: Location) {
    this.loading = true;
    this.http.put(`https://api.snglrtycrvtureofspce.me/Location/${id}`, location)
      .pipe(
        catchError(error => {
          console.error('Ошибка при обновлении местоположения:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Местоположение успешно обновлено" });
          this.getLocations();
          this.editingLocation = null;
        })
      )
      .subscribe();
  }

  deleteLocation(id: string) {
    this.loading = true;
    this.http.delete(`https://api.snglrtycrvtureofspce.me/Location/${id}`)
      .pipe(
        catchError(error => {
          console.error('Ошибка при удалении местоположения:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Местоположение успешно удалено" });
          this.getLocations();
        })
      )
      .subscribe();
  }

  openEditModal(location: Location) {
    this.editingLocation = { ...location };
  }

  cancelEdit() {
    this.editingLocation = null;
  }

  FilterFn() {
    const NameFilter = this.NameFilter.toLowerCase().trim();
    const DescriptionFilter = this.DescriptionFilter.toLowerCase().trim();
    const LatitudeFilter = this.LatitudeFilter.toLowerCase().trim();
    const LongitudeFilter = this.LongitudeFilter.toLowerCase().trim();

    this.locations = this.locationsWithoutFilter.filter(el => {
      return (
        el.name.toLowerCase().includes(NameFilter) &&
        el.description.toLowerCase().includes(DescriptionFilter) &&
        el.latitude.toString().toLowerCase().includes(LatitudeFilter) &&
        el.longitude.toString().toLowerCase().includes(LongitudeFilter)
      );
    });
  }

  sortResult(prop: keyof Location, asc: boolean) {
    this.sortColumn = prop;
    this.sortAsc = asc;
    this.locations.sort((a, b) => {
      if (asc) {
        return a[prop] > b[prop] ? 1 : a[prop] < b[prop] ? -1 : 0;
      } else {
        return b[prop] > a[prop] ? 1 : b[prop] < a[prop] ? -1 : 0;
      }
    });
  }
}