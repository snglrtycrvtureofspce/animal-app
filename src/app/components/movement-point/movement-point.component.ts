import { Component, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize, switchMap } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { NgToastService } from 'ng-angular-popup';

interface MovementPoint {
  id: string;
  latitude: number;
  longitude: number;
  verified: boolean;
  animalId: string;
  locationId: string;
  userId: string;
}

interface Animal {
  id: string;
  createdDate: string;
  modificationDate: string;
  name: string;
  description: string;
  squad: string;
  family: string;
  rod: string;
  view: string;
  internationalScientificName: string;
  encyclopediaUrl: string;
  population: number;
  photoUrl: string;
  animalClassId: string;
}

interface Location {
  id: string;
  createdDate: string;
  modificationDate: string;
  name: string;
  description: string;
  latitude: number;
  longitude: number;
}

interface User {
  id: string;
  userName: string;
  email: string;
  firstName: string;
  lastName: string;
  middleName: string;
  dateOfBirth: Date;
  country: string;
  city: string;
  language: string;
  userPhoto: string;
  facebookLink: string;
  instagramLink: string;
  twitterLink: string;
  vkLink: string;
  site: string;
}

@Component({
  selector: 'app-movement-point',
  templateUrl: './movement-point.component.html',
  styleUrls: ['./movement-point.component.scss']
})
export class MovementPointComponent implements OnInit {
  isAdmin: boolean = false;
  movementPoints: MovementPoint[] = [];
  movementPointsWithoutFilter: MovementPoint[] = [];
  animals: Animal[] = [];
  locations: Location[] = [];
  newMovementPoint: Partial<MovementPoint> = {};
  editingMovementPoint: MovementPoint | null = null;
  loading: boolean = false;
  showCreateModal: boolean = false;

  LatitudeFilter: string = '';
  LongitudeFilter: string = '';
  VerifiedFilter: string = '';
  AnimalNameFilter: string = '';
  LocationNameFilter: string = '';
  sortColumn: keyof MovementPoint = 'latitude';
  sortAsc: boolean = true;

  constructor(private http: HttpClient, private toast: NgToastService, private auth: AuthService) { }

  ngOnInit(): void {
    this.isAdmin = this.auth.isAdmin();
    this.getMovementPoints();
    this.getAnimals();
    this.getLocations();
  }

  showScrollButton: boolean = false;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (window.pageYOffset > 300) {
      this.showScrollButton = true;
    } else {
      this.showScrollButton = false;
    }
  }

  scrollToTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  getMovementPoints() {
    this.loading = true;
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: MovementPoint[] }>('https://api.snglrtycrvtureofspce.me/MovementPoint')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка точек перемещения:', error);
          return EMPTY;
        }),
        finalize(() => this.loading = false)
      )
      .subscribe(response => {
        this.movementPoints = response.elements;
        this.movementPointsWithoutFilter = response.elements;
      });
  }

  getAnimals() {
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: Animal[] }>('https://api.snglrtycrvtureofspce.me/Animal')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка животных:', error);
          return EMPTY;
        })
      )
      .subscribe(response => {
        this.animals = response.elements;
      });
  }

  getLocations() {
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: Location[] }>('https://api.snglrtycrvtureofspce.me/Location')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка местоположений:', error);
          return EMPTY;
        })
      )
      .subscribe(response => {
        this.locations = response.elements;
      });
  }

  createMovementPoint() {
    if (!this.newMovementPoint.latitude || !this.newMovementPoint.longitude || !this.newMovementPoint.animalId || !this.newMovementPoint.locationId) {
      return;
    }
    this.loading = true;
    this.http.post('https://api.snglrtycrvtureofspce.me/MovementPoint', this.newMovementPoint)
      .pipe(
        catchError(error => {
          console.error('Ошибка при создании точки перемещения:', error);
          return EMPTY;
        }),
        finalize(() => {
          if (!this.isAdmin) {
            this.toast.success({ detail: "Успех", summary: "Точка перемещения отправлена на верификацию" });
          } else {
            this.toast.success({ detail: "Успех", summary: "Точка перемещения успешно создана" });
          }
          this.getMovementPoints();
          this.newMovementPoint = {};
          this.showCreateModal = false;
        })
      )
      .subscribe();
  }

  updateMovementPoint(id: string, movementPoint: MovementPoint) {
    this.loading = true;
    this.http.put(`https://api.snglrtycrvtureofspce.me/MovementPoint/${id}`, movementPoint)
      .pipe(
        catchError(error => {
          console.error('Ошибка при обновлении точки перемещения:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Точка перемещения успешно обновлена" });
          this.getMovementPoints();
          this.editingMovementPoint = null;
        })
      )
      .subscribe();
  }

  deleteMovementPoint(id: string) {
    this.loading = true;
    this.http.delete(`https://api.snglrtycrvtureofspce.me/MovementPoint/${id}`)
      .pipe(
        catchError(error => {
          console.error('Ошибка при удалении точки перемещения:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Точка перемещения успешно удалена" });
          this.getMovementPoints();
        })
      )
      .subscribe();
  }

  getAnimalName(id: string): string {
    const animal = this.animals.find(a => a.id === id);
    return animal ? animal.name : 'Неизвестно';
  }

  getLocationName(id: string): string {
    const location = this.locations.find(l => l.id === id);
    return location ? location.name : 'Неизвестно';
  }

  getVerifiedStatus(verified: boolean): string {
    return verified ? 'Подтверждено' : 'Не подтверждено';
  }

  private sendEmail(to: string, subject: string, body: string, userId: string) {
    const emailData = {
      to,
      subject,
      body,
      isBodyHtml: true,
      userId
    };
    return this.http.post('https://api.snglrtycrvtureofspce.me/Mail', emailData)
      .pipe(
        catchError(error => {
          console.error('Ошибка при отправке почты:', error);
          return EMPTY;
        })
      );
  }

  private getUserEmail(userId: string) {
    return this.http.get<{ item: User }>(`https://api.snglrtycrvtureofspce.me/User/${userId}`)
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении пользователя:', error);
          return EMPTY;
        })
      );
  }

  verifyMovementPoint(id: string, userId: string) {
    this.loading = true;
    const movementPoint = this.movementPoints.find(mp => mp.id === id);
    if (!movementPoint) {
      this.toast.error({ detail: "Ошибка", summary: "Точка перемещения не найдена" });
      return;
    }

    this.getUserEmail(userId).pipe(
      switchMap(response => {
        const { email } = response.item;
        if (!email) {
          this.toast.error({ detail: "Ошибка", summary: "Пользователь не имеет email" });
          this.getMovementPoints();
        }
        const emailBody = `
          <p>Ваша точка перемещения успешно верифицирована.</p>
          <p><strong>Данные точки перемещения:</strong></p>
          <p>Широта: ${movementPoint.latitude}</p>
          <p>Долгота: ${movementPoint.longitude}</p>
          <p>Животное: ${this.getAnimalName(movementPoint.animalId)}</p>
          <p>Местоположение: ${this.getLocationName(movementPoint.locationId)}</p>
        `;
        return this.http.put(`https://api.snglrtycrvtureofspce.me/MovementPointVerification/${id}`, { newStatus: true }).pipe(
          switchMap(() => this.sendEmail(email, 'Точка перемещения верифицирована', emailBody, userId)),
          catchError(error => {
            console.error('Ошибка при верификации точки перемещения:', error);
            return EMPTY;
          }),
          finalize(() => {
            this.toast.success({ detail: "Успех", summary: "Точка перемещения успешно верифицирована" });
            this.getMovementPoints();
          })
        );
      })
    ).subscribe();
  }

  unVerifyMovementPoint(id: string, userId: string) {
    this.loading = true;
    const movementPoint = this.movementPoints.find(mp => mp.id === id);
    if (!movementPoint) {
      this.toast.error({ detail: "Ошибка", summary: "Точка перемещения не найдена" });
      return;
    }

    this.getUserEmail(userId).pipe(
      switchMap(response => {
        const { email } = response.item;
        if (!email) {
          console.error('Пользователь не имеет email');
          return EMPTY;
        }
        const emailBody = `
          <p>Ваша точка перемещения не прошла верификацию в связи с некорректными данными.</p>
          <p><strong>Данные точки перемещения:</strong></p>
          <p>Широта: ${movementPoint.latitude}</p>
          <p>Долгота: ${movementPoint.longitude}</p>
          <p>Животное: ${this.getAnimalName(movementPoint.animalId)}</p>
          <p>Местоположение: ${this.getLocationName(movementPoint.locationId)}</p>
        `;
        return this.http.put(`https://api.snglrtycrvtureofspce.me/MovementPointVerification/${id}`, { newStatus: false }).pipe(
          switchMap(() => this.sendEmail(email, 'Точка перемещения не прошла верификацию', emailBody, userId).pipe(
            finalize(() => {
              this.deleteMovementPoint(id);
            })
          )),
          catchError(error => {
            console.error('Ошибка при аннулировании точки перемещения:', error);
            return EMPTY;
          }),
          finalize(() => {
            this.toast.success({ detail: "Успех", summary: "Точка перемещения не прошла верификацию и была удалена" });
            this.getMovementPoints();
          })
        );
      })
    ).subscribe();
  }

  openEditModal(movementPoint: MovementPoint) {
    this.editingMovementPoint = { ...movementPoint };
  }

  cancelEdit() {
    this.editingMovementPoint = null;
  }

  FilterFn() {
    const LatitudeFilter = this.LatitudeFilter.toLowerCase().trim();
    const LongitudeFilter = this.LongitudeFilter.toLowerCase().trim();
    const VerifiedFilter = this.VerifiedFilter.toLowerCase().trim();
    const AnimalNameFilter = this.AnimalNameFilter.toLowerCase().trim();
    const LocationNameFilter = this.LocationNameFilter.toLowerCase().trim();

    this.movementPoints = this.movementPointsWithoutFilter.filter(el => {
      const animalName = this.getAnimalName(el.animalId).toLowerCase();
      const locationName = this.getLocationName(el.locationId).toLowerCase();
      const verified = this.getVerifiedStatus(el.verified).toLowerCase();

      return (
        el.latitude.toString().toLowerCase().includes(LatitudeFilter) &&
        el.longitude.toString().toLowerCase().includes(LongitudeFilter) &&
        animalName.includes(AnimalNameFilter) &&
        locationName.includes(LocationNameFilter) &&
        verified.includes(VerifiedFilter)
      );
    });
  }

  sortResult(prop: keyof MovementPoint, asc: boolean) {
    this.sortColumn = prop;
    this.sortAsc = asc;
    this.movementPoints.sort((a, b) => {
      if (asc) {
        return a[prop] > b[prop] ? 1 : a[prop] < b[prop] ? -1 : 0;
      } else {
        return b[prop] > a[prop] ? 1 : b[prop] < a[prop] ? -1 : 0;
      }
    });
  }
}