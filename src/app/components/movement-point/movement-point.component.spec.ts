import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementPointComponent } from './movement-point.component';

describe('MovementPointComponent', () => {
  let component: MovementPointComponent;
  let fixture: ComponentFixture<MovementPointComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MovementPointComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(MovementPointComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
