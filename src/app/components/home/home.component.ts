import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-home',
  standalone: true,
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements AfterViewInit {
  @ViewChild('backgroundVideo') backgroundVideo!: ElementRef<HTMLVideoElement>;
  isPaused: boolean = false;

  ngAfterViewInit() {
    if (this.backgroundVideo && this.backgroundVideo.nativeElement) {
      this.backgroundVideo.nativeElement.muted = true;
    }
  }

  toggleVideo() {
    if (this.backgroundVideo && this.backgroundVideo.nativeElement) {
      if (this.isPaused) {
        this.backgroundVideo.nativeElement.play();
      } else {
        this.backgroundVideo.nativeElement.pause();
      }
      this.isPaused = !this.isPaused;
    }
  }
}