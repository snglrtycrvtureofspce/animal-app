import { Component, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { countries } from './../../models/countries';
import { cities } from './../../models/cities';
import { AuthService } from '../../services/auth.service'
import { NgToastService } from 'ng-angular-popup';

interface User {
  id: string;
  userName: string;
  email: string;
  firstName: string;
  lastName: string;
  middleName: string;
  dateOfBirth: Date | string;
  country: string;
  city: string;
  language: string;
  userPhoto: string;
  facebookLink: string;
  instagramLink: string;
  twitterLink: string;
  vkLink: string;
  site: string;
}

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  user: User | null = null;
  editingUser: User | null = null;
  countries: string[] = countries;
  cities: { [key: string]: string[] } = cities;
  selectedFile: File | null = null;
  showCreateModal: boolean = false;
  showDeleteConfirmation: boolean = false;
  loading: boolean = false;
  selectedCountry: string | null = null;

  defaultPhoto: string = '../../../assets/user-profile-icon.jpg';

  constructor(private http: HttpClient, private toast: NgToastService, private authService: AuthService) { }

  ngOnInit(): void {
    this.getUser();
  }

  showScrollButton: boolean = false;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (window.pageYOffset > 300) {
      this.showScrollButton = true;
    } else {
      this.showScrollButton = false;
    }
  }

  scrollToTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  getUserId(): string | null {
    return localStorage.getItem('userId');
  }

  getUser() {
    const userId = this.getUserId();
    if (userId) {
      this.loading = true;
      this.http.get<{ item: User }>(`https://api.snglrtycrvtureofspce.me/User/${userId}`)
        .pipe(
          catchError(error => {
            console.error('Ошибка при получении пользователя:', error);
            return EMPTY;
          }),
          finalize(() => this.loading = false)
        )
        .subscribe(response => {
          if (response && response.item) {
            this.user = response.item;
            if (!this.user.userPhoto) {
              this.user.userPhoto = this.defaultPhoto;
            }
          } else {
            this.user = null;
          }
        });
    } else {
      console.error('Пользователь не найден в локальном хранилище');
    }
  }

  updateUser(id: string, user: User) {
    const formData = new FormData();
    formData.append('userName', user.userName || '');
    formData.append('email', user.email || '');
    formData.append('firstName', user.firstName || '');
    formData.append('lastName', user.lastName || '');
    formData.append('middleName', user.middleName || '');
    formData.append('dateOfBirth', user.dateOfBirth ? new Date(user.dateOfBirth).toISOString() : '');
    formData.append('country', user.country || '');
    formData.append('city', user.city || '');
    formData.append('language', user.language || '');
    formData.append('facebookLink', user.facebookLink || '');
    formData.append('instagramLink', user.instagramLink || '');
    formData.append('twitterLink', user.twitterLink || '');
    formData.append('vkLink', user.vkLink || '');
    formData.append('site', user.site || '');
    
    // Проверяем наличие нового изображения или сохраняем текущее
    if (this.selectedFile) {
        formData.append('userPhoto', this.selectedFile);
    } else if (user.userPhoto) {
        formData.append('userPhoto', user.userPhoto);
    }
  
    this.loading = true;
    this.http.put(`https://api.snglrtycrvtureofspce.me/User/${id}`, formData)
      .pipe(
        catchError(error => {
          console.error('Ошибка при обновлении пользователя:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Пользователь успешно обновлен" });
          this.getUser();
          this.editingUser = null;
        })
      )
      .subscribe();
  }

  deleteUser(id: string) {
    this.loading = true;
    this.http.delete(`https://api.snglrtycrvtureofspce.me/User/${id}`)
      .pipe(
        catchError(error => {
          console.error('Ошибка при удалении пользователя:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Пользователь успешно удален" });
          this.user = null;
        })
      )
      .subscribe();
  }

  removePhoto() {
    if (this.editingUser) {
      this.editingUser.userPhoto = this.defaultPhoto;
    }
  }

  openEditModal(user: User) {
    this.editingUser = { ...user };
    if (this.editingUser.dateOfBirth) {
      const date = new Date(this.editingUser.dateOfBirth);
      const year = date.getFullYear();
      const month = ('0' + (date.getMonth() + 1)).slice(-2);
      const day = ('0' + date.getDate()).slice(-2);
      this.editingUser.dateOfBirth = `${year}-${month}-${day}`;
    }
  }

  cancelEdit() {
    this.editingUser = null;
  }

  signOut(): void {
    this.authService.signOut();
  }

  confirmDeleteUser() {
    this.showDeleteConfirmation = true;
  }

  deleteConfirmed() {
    if (this.user) {
      this.deleteUser(this.user.id);
      this.authService.signOut();
    }
    this.showDeleteConfirmation = false;
  }

  cancelDelete() {
    this.showDeleteConfirmation = false;
  }

  onFileSelected(event: any) {
    const file = event.target.files[0];
    const allowedTypes = ['image/jpeg', 'image/png', 'image/gif', 'image/bmp', 'image/webp'];
  
    if (file && allowedTypes.includes(file.type)) {
      this.selectedFile = file;
    } else {
      this.toast.error({ detail: "Ошибка", summary: "Недопустимый формат файла. Пожалуйста, выберите изображение." });
    }
  }

  onCountryChange() {
    if (this.editingUser) {
      this.editingUser.city = '';
    }
  }
}