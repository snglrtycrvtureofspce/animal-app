import { Component, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { NgToastService } from 'ng-angular-popup';

interface AnimalClass {
  id: string;
  createdDate: string;
  modificationDate: string;
  name: string;
  description: string;
  domain: string;
  realm: string;
  type: string;
  internationalScientificName: string;
  encyclopediaUrl: string;
}

@Component({
  selector: 'app-animal-class',
  templateUrl: './animal-class.component.html',
  styleUrls: ['./animal-class.component.scss']
})
export class AnimalClassComponent implements OnInit {
  isAdmin: boolean = false;
  loading: boolean = false;
  animalClasses: AnimalClass[] = [];
  animalClassesWithoutFilter: AnimalClass[] = [];
  newAnimalClass: Partial<AnimalClass> = {};
  editingAnimalClass: AnimalClass | null = null;
  showCreateModal: boolean = false;

  AnimalClassNameFilter: string = '';
  AnimalClassDescriptionFilter: string = '';
  AnimalClassDomainFilter: string = '';
  AnimalClassRealmFilter: string = '';
  AnimalClassTypeFilter: string = '';
  AnimalClassInternationalScientificNameFilter: string = '';
  AnimalClassEncyclopediaUrlFilter: string = '';
  sortColumn: keyof AnimalClass = 'name';
  sortAsc: boolean = true;

  constructor(private http: HttpClient, private toast: NgToastService, private auth: AuthService) { }

  ngOnInit(): void {
    this.isAdmin = this.auth.isAdmin();
    this.getAnimalСlasses();
  }

  showScrollButton: boolean = false;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (window.pageYOffset > 300) {
      this.showScrollButton = true;
    } else {
      this.showScrollButton = false;
    }
  }

  scrollToTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  getAnimalСlasses() {
    this.loading = true;
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: AnimalClass[] }>('https://api.snglrtycrvtureofspce.me/AnimalClass')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка классов животных:', error);
          return EMPTY;
        }),
        finalize(() => this.loading = false)
      )
      .subscribe(response => {
        this.animalClasses = response.elements;
        this.animalClassesWithoutFilter = response.elements;
      });
  }

  createAnimalClass() {
    if (!this.newAnimalClass.name || !this.newAnimalClass.description || !this.newAnimalClass.domain || !this.newAnimalClass.realm || !this.newAnimalClass.type || !this.newAnimalClass.internationalScientificName || !this.newAnimalClass.encyclopediaUrl) {
      return;
    }
    this.loading = true;
    this.http.post('https://api.snglrtycrvtureofspce.me/AnimalClass', this.newAnimalClass)
      .pipe(
        catchError(error => {
          console.error('Ошибка при создании класса животного:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Класс животного успешно создан" });
          this.getAnimalСlasses();
          this.newAnimalClass = {};
          this.showCreateModal = false;
        })
      )
      .subscribe();
  }

  updateAnimalClass(id: string, animalClass: AnimalClass) {
    this.loading = true;
    this.http.put(`https://api.snglrtycrvtureofspce.me/AnimalClass/${id}`, animalClass)
      .pipe(
        catchError(error => {
          console.error('Ошибка при обновлении класса животного:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Класс животного успешно обновлен" });
          this.getAnimalСlasses();
          this.editingAnimalClass = null;
        })
      )
      .subscribe();
  }

  deleteAnimalClass(id: string) {
    this.loading = true;
    this.http.delete(`https://api.snglrtycrvtureofspce.me/AnimalClass/${id}`)
      .pipe(
        catchError(error => {
          console.error('Ошибка при удалении класса животного:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Класс животного успешно удален" });
          this.getAnimalСlasses();
        })
      )
      .subscribe();
  }

  openEditModal(animalClass: AnimalClass) {
    this.editingAnimalClass = { ...animalClass };
  }

  cancelEdit() {
    this.editingAnimalClass = null;
  }

  FilterFn() {
    const AnimalClassNameFilter = this.AnimalClassNameFilter.toLowerCase().trim();
    const AnimalClassDescriptionFilter = this.AnimalClassDescriptionFilter.toLowerCase().trim();
    const AnimalClassDomainFilter = this.AnimalClassDomainFilter.toLowerCase().trim();
    const AnimalClassRealmFilter = this.AnimalClassRealmFilter.toLowerCase().trim();
    const AnimalClassTypeFilter = this.AnimalClassTypeFilter.toLowerCase().trim();
    const AnimalClassInternationalScientificNameFilter = this.AnimalClassInternationalScientificNameFilter.toLowerCase().trim();
    const AnimalClassEncyclopediaUrlFilter = this.AnimalClassEncyclopediaUrlFilter.toLowerCase().trim();

    this.animalClasses = this.animalClassesWithoutFilter.filter(el => {
      return (
        el.name.toLowerCase().includes(AnimalClassNameFilter) &&
        el.description.toLowerCase().includes(AnimalClassDescriptionFilter) &&
        el.domain.toLowerCase().includes(AnimalClassDomainFilter) &&
        el.realm.toLowerCase().includes(AnimalClassRealmFilter) &&
        el.type.toLowerCase().includes(AnimalClassTypeFilter) &&
        el.internationalScientificName.toLowerCase().includes(AnimalClassInternationalScientificNameFilter) &&
        el.encyclopediaUrl.toLowerCase().includes(AnimalClassEncyclopediaUrlFilter)
      );
    });
  }

  sortResult(prop: keyof AnimalClass, asc: boolean) {
    this.sortColumn = prop;
    this.sortAsc = asc;
    this.animalClasses.sort((a, b) => {
      if (asc) {
        return a[prop] > b[prop] ? 1 : a[prop] < b[prop] ? -1 : 0;
      } else {
        return b[prop] > a[prop] ? 1 : b[prop] < a[prop] ? -1 : 0;
      }
    });
  }
}