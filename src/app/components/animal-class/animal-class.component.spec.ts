import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimalClassComponent } from './animal-class.component';

describe('AnimalClassComponent', () => {
  let component: AnimalClassComponent;
  let fixture: ComponentFixture<AnimalClassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AnimalClassComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(AnimalClassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
