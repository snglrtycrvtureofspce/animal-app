import { Component, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { NgToastService } from 'ng-angular-popup';

interface Animal {
  id: string;
  createdDate: string;
  modificationDate: string;
  name: string;
  description: string;
  squad: string;
  family: string;
  rod: string;
  view: string;
  internationalScientificName: string;
  encyclopediaUrl: string;
  population: number;
  photoUrl: string;
  animalClassId: string;
}

interface AnimalClass {
  id: string;
  createdDate: string;
  modificationDate: string;
  name: string;
  description: string;
  domain: string;
  realm: string;
  type: string;
  internationalScientificName: string;
  encyclopediaUrl: string;
}

interface Location {
  id: string;
  createdDate: string;
  modificationDate: string;
  name: string;
  description: string;
  latitude: number;
  longitude: number;
}

interface MovementPoint {
  id: string;
  latitude: number;
  longitude: number;
  animalId: string;
  userId: string;
  locationId: string;
  verified: boolean;
}

interface Report {
  animalType: string;
  animalCount: number;
  movementCount: number;
  topLocations: { [key: string]: number };
  animals: AnimalWithMovements[];
  animalMovementsSummary: { animalName: string, movementLocations: string[] }[];
}

interface AnimalWithMovements extends Animal {
  movements: Location[];
}

@Component({
  selector: 'app-animal-report',
  templateUrl: './animal-report.component.html',
  styleUrls: ['./animal-report.component.scss']
})
export class AnimalReportComponent implements OnInit {
  isAdmin: boolean = false;
  loading: boolean = false;
  animals: Animal[] = [];
  animalClasses: AnimalClass[] = [];
  locations: Location[] = [];
  movementPoints: MovementPoint[] = [];
  reports: Report[] = [];

  constructor(private http: HttpClient, private toast: NgToastService, private auth: AuthService) { }

  ngOnInit(): void {
    this.isAdmin = this.auth.isAdmin();
    this.getAnimals();
    this.getAnimalClasses();
    this.getLocations();
    this.getMovementPoints();
  }

  showScrollButton: boolean = false;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (window.pageYOffset > 300) {
      this.showScrollButton = true;
    } else {
      this.showScrollButton = false;
    }
  }

  scrollToTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  getAnimals() {
    this.loading = true;
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: Animal[] }>('https://api.snglrtycrvtureofspce.me/Animal')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка животных:', error);
          return EMPTY;
        }),
        finalize(() => this.loading = false)
      )
      .subscribe(response => {
        this.animals = response.elements;
        this.generateReports();
      });
  }

  getAnimalClasses() {
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: AnimalClass[] }>('https://api.snglrtycrvtureofspce.me/AnimalClass')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка типов животных:', error);
          return EMPTY;
        })
      )
      .subscribe(response => {
        this.animalClasses = response.elements;
        this.generateReports();
      });
  }

  getLocations() {
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: Location[] }>('https://api.snglrtycrvtureofspce.me/Location')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка местоположений:', error);
          return EMPTY;
        })
      )
      .subscribe(response => {
        this.locations = response.elements;
      });
  }

  getMovementPoints() {
    this.loading = true;
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: MovementPoint[] }>('https://api.snglrtycrvtureofspce.me/MovementPoint')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка точек перемещения:', error);
          return EMPTY;
        }),
        finalize(() => this.loading = false)
      )
      .subscribe(response => {
        this.movementPoints = response.elements;
        this.generateReports();
      });
  }

  generateReports() {
    if (!this.animals.length || !this.animalClasses.length || !this.movementPoints.length || !this.locations.length) {
      return;
    }

    this.reports = this.animalClasses.map(type => {
      const animalsOfType = this.animals
        .filter(animal => animal.animalClassId === type.id)
        .map(animal => {
          const movements = this.movementPoints
            .filter(point => point.animalId === animal.id)
            .map(point => this.locations.find(location => location.id === point.locationId))
            .filter(location => location !== undefined) as Location[];
          return { ...animal, movements };
        });

      const animalCount = animalsOfType.reduce((sum, animal) => sum + animal.population, 0);
      const movementCount = this.movementPoints.filter(point => animalsOfType.some(animal => animal.id === point.animalId)).length;

      const topLocations = this.movementPoints
        .filter(point => animalsOfType.some(animal => animal.id === point.animalId))
        .reduce((locationMap, point) => {
          if (!locationMap[point.locationId]) {
            locationMap[point.locationId] = 0;
          }
          locationMap[point.locationId]++;
          return locationMap;
        }, {} as { [key: string]: number });

      const animalMovementsSummary = animalsOfType
        .filter(animal => animal.movements.length > 0)
        .map(animal => ({
          animalName: animal.name,
          movementLocations: animal.movements.map(movement => this.getLocationName(movement.id))
        }));

      return {
        animalType: type.name,
        animalCount,
        movementCount,
        topLocations,
        animals: animalsOfType,
        animalMovementsSummary
      };
    });

    if (this.reports.length === 0) {
      this.toast.info({ detail: "Информация", summary: "Нет данных для формирования отчетов", duration: 3000 });
    }
  }

  getLocationName(locationId: string): string {
    const location = this.locations.find(loc => loc.id === locationId);
    return location ? location.name : 'Неизвестно';
  }

  getTopLocationIds(topLocations: { [key: string]: number }): string[] {
    return Object.keys(topLocations).sort((a, b) => topLocations[b] - topLocations[a]).slice(0, 5);
  }
}
