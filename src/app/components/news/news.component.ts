import { Component, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';
import { EMPTY, forkJoin } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { NgToastService } from 'ng-angular-popup';
import { DatePipe } from '@angular/common';

interface News {
  id: string;
  createdDate: string | null;
  modificationDate: string;
  title: string;
  content: string;
  photoUrl: string;
}

interface User {
  id: string;
  userName: string;
  email: string;
  firstName: string;
  lastName: string;
  middleName: string;
  dateOfBirth: Date;
  country: string;
  city: string;
  language: string;
  userPhoto: string;
  facebookLink: string;
  instagramLink: string;
  twitterLink: string;
  vkLink: string;
  site: string;
}

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  providers: [DatePipe]
})
export class NewsComponent implements OnInit {
  isAdmin: boolean = false;
  loading: boolean = false;
  news: News[] = [];
  paginatedNews: News[][] = [];
  currentPage: number = 0;
  itemsPerPage: number = 9;
  newNews: Partial<News> = {};
  editingNews: News | null = null;
  showCreateModal: boolean = false;

  constructor(private http: HttpClient, private auth: AuthService, private toast: NgToastService, private datePipe: DatePipe) { }

  ngOnInit(): void {
    this.isAdmin = this.auth.isAdmin();
    this.getNews();
  }

  showScrollButton: boolean = false;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (window.pageYOffset > 300) {
      this.showScrollButton = true;
    } else {
      this.showScrollButton = false;
    }
  }

  scrollToTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  getNews() {
    this.loading = true;
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: News[] }>('https://api.snglrtycrvtureofspce.me/News')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении новостей:', error);
          return EMPTY;
        }),
        finalize(() => this.loading = false)
      )
      .subscribe(response => {
        this.news = response.elements.map(item => ({
          ...item,
          createdDate: this.datePipe.transform(item.createdDate, 'dd.MM.yyyy') || ''
        }));
        this.paginateNews();
      });
  }

  paginateNews() {
    this.paginatedNews = [];
    for (let i = 0; i < this.news.length; i += this.itemsPerPage) {
      this.paginatedNews.push(this.news.slice(i, i + this.itemsPerPage));
    }
  }

  prevPage() {
    if (this.currentPage > 0) {
      this.currentPage--;
      this.scrollToTop();
    }
  }

  nextPage() {
    if (this.currentPage < this.paginatedNews.length - 1) {
      this.currentPage++;
      this.scrollToTop();
    }
  }

  private getAllUsers() {
    return this.http.get<{ users: User[] }>('https://api.snglrtycrvtureofspce.me/User')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении пользователей:', error);
          return EMPTY;
        })
      );
  }

  private sendEmail(to: string, subject: string, body: string, userId: string) {
    const emailData = {
      to,
      subject,
      body,
      isBodyHtml: true,
      userId
    };
    return this.http.post('https://api.snglrtycrvtureofspce.me/Mail', emailData)
      .pipe(
        catchError(error => {
          console.error('Ошибка при отправке почты:', error);
          return EMPTY;
        })
      );
  }

  private sendEmailsToAllUsers(news: News) {
    this.getAllUsers().subscribe(response => {
      const users = response.users;
      const emailRequests = users.map(user => {
        const subject = `Новая новость: ${news.title}`;
        const body = `<h1>${news.title}</h1><p>${news.content}</p>`;
        return this.sendEmail(user.email, subject, body, user.id);
      });

      forkJoin(emailRequests).subscribe(() => {
        this.toast.success({ detail: "Успех", summary: "Письма успешно отправлены всем пользователям" });
      });
    });
  }

  createNews() {
    if (!this.newNews.title || !this.newNews.content || !this.newNews.photoUrl) {
      return;
    }
    this.loading = true;
    this.http.post<{ item: News }>('https://api.snglrtycrvtureofspce.me/News', this.newNews)
      .pipe(
        catchError(error => {
          console.error('Ошибка при создании новости:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Новость успешно создана" });
          this.getNews();
          this.newNews = {};
          this.showCreateModal = false;
        })
      )
      .subscribe(response => {
        const createdNews = response.item;
        this.sendEmailsToAllUsers(createdNews);
      });
  }

  updateNews(id: string, news: News) {
    this.loading = true;
    this.http.put(`https://api.snglrtycrvtureofspce.me/News/${id}`, news)
      .pipe(
        catchError(error => {
          console.error('Ошибка при обновлении новости:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Новость успешно обновлена" });
          this.getNews();
          this.editingNews = null;
        })
      )
      .subscribe();
  }

  deleteNews(id: string) {
    this.loading = true;
    this.http.delete(`https://api.snglrtycrvtureofspce.me/News/${id}`)
      .pipe(
        catchError(error => {
          console.error('Ошибка при удалении новостей:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Новость успешно удалена" });
          this.getNews();
        })
      )
      .subscribe();
  }

  openEditModal(news: News) {
    this.editingNews = { ...news };
  }

  cancelEdit() {
    this.editingNews = null;
  }

  onFileChange(event: Event, mode: 'create' | 'edit') {
    const input = event.target as HTMLInputElement;
    if (input.files && input.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        if (mode === 'create') {
          this.newNews.photoUrl = e.target.result;
        } else {
          this.editingNews!.photoUrl = e.target.result;
        }
      };
      reader.readAsDataURL(input.files[0]);
    }
  }
}
