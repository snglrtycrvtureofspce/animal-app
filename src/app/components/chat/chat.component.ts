import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { NgToastService } from 'ng-angular-popup';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent {
  title: string = '';
  content: string = '';
  isFormVisible: boolean = false;

  constructor(private http: HttpClient, private authService: AuthService, private toast: NgToastService) { }

  toggleForm() {
    this.isFormVisible = !this.isFormVisible;
  }

  getUserId() {
    return this.authService.getUserId();
  }

  sendMessage() {
    const userId = this.getUserId();
    if (userId) {
      this.sendEmail('snglrtycrvtureofspce@gmail.com', this.title, `<p><strong>${this.content}</strong></p>`, userId).subscribe(
        response => {
          this.toast.success({ detail: "Успех", summary: "Предложение по улучшению приложения было успешно отправлено!" });
          this.title = '';
          this.content = '';
          this.isFormVisible = false;
        },
        error => {
          console.error('Ошибка при отправке почты:', error);
        }
      );
    } else {
      this.toast.error({ detail: "Ошибка", summary: "Пожалуйста авторизируйтесь!" });
    }
  }

  private sendEmail(to: string, subject: string, body: string, userId: string) {
    const emailData = {
      to,
      subject,
      body,
      isBodyHtml: true,
      userId
    };
    return this.http.post('https://api.snglrtycrvtureofspce.me/Mail', emailData)
      .pipe(
        catchError(error => {
          console.error('Ошибка при отправке почты:', error);
          return EMPTY;
        })
      );
  }
}