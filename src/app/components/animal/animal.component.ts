import { Component, OnInit, HostListener } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';
import { EMPTY } from 'rxjs';
import { AuthService } from '../../services/auth.service';
import { NgToastService } from 'ng-angular-popup';

interface Animal {
  id: string;
  createdDate: string;
  modificationDate: string;
  name: string;
  description: string;
  squad: string;
  family: string;
  rod: string;
  view: string;
  internationalScientificName: string;
  encyclopediaUrl: string;
  population: number;
  photoUrl: string;
  animalClassId: string;
}

interface AnimalClass {
  id: string;
  createdDate: string;
  modificationDate: string;
  name: string;
  description: string;
  domain: string;
  realm: string;
  type: string;
  internationalScientificName: string;
  encyclopediaUrl: string;
}

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.scss']
})
export class AnimalComponent implements OnInit {
  isAdmin: boolean = false;
  loading: boolean = false;
  animals: Animal[] = [];
  animalsWithoutFilter: Animal[] = [];
  animalClasses: AnimalClass[] = [];
  newAnimal: Partial<Animal> = {};
  editingAnimal: Animal | null = null;
  showCreateModal: boolean = false;

  AnimalNameFilter: string = '';
  AnimalDescriptionFilter: string = '';
  AnimalSquadFilter: string = '';
  AnimalFamilyFilter: string = '';
  AnimalRodFilter: string = '';
  AnimalViewFilter: string = '';
  AnimalInternationalScientificNameFilter: string = '';
  AnimalEncyclopediaUrlFilter: string = '';
  AnimalPopulationFilter: string = '';
  AnimalPhotoUrlFilter: string = '';
  AnimalClassNameFilter: string = '';
  sortColumn: keyof Animal = 'name';
  sortAsc: boolean = true;

  constructor(private http: HttpClient, private toast: NgToastService, private auth: AuthService) { }

  ngOnInit(): void {
    this.isAdmin = this.auth.isAdmin();
    this.getAnimals();
    this.getAnimalClasses();
  }

  showScrollButton: boolean = false;

  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (window.pageYOffset > 300) {
      this.showScrollButton = true;
    } else {
      this.showScrollButton = false;
    }
  }

  scrollToTop() {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  }

  getAnimals() {
    this.loading = true;
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: Animal[] }>('https://api.snglrtycrvtureofspce.me/Animal')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка животных:', error);
          return EMPTY;
        }),
        finalize(() => this.loading = false)
      )
      .subscribe(response => {
        this.animals = response.elements;
        this.animalsWithoutFilter = response.elements;;
      });
  }

  getAnimalClassName(id: string): string {
    const animalClass = this.animalClasses.find(t => t.id === id);
    return animalClass ? animalClass.name : 'Неизвестно';
  }

  getAnimalClasses() {
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: AnimalClass[] }>('https://api.snglrtycrvtureofspce.me/AnimalClass')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка классов животных:', error);
          return EMPTY;
        })
      )
      .subscribe(response => {
        this.animalClasses = response.elements;
      });
  }

  createAnimal() {
    if (!this.newAnimal.name || !this.newAnimal.description || !this.newAnimal.squad || !this.newAnimal.family || !this.newAnimal.rod || !this.newAnimal.view || !this.newAnimal.internationalScientificName || !this.newAnimal.encyclopediaUrl || !this.newAnimal.population || !this.newAnimal.photoUrl || !this.newAnimal.animalClassId) {
      return;
    }
    this.loading = true;
    this.http.post('https://api.snglrtycrvtureofspce.me/Animal', this.newAnimal)
      .pipe(
        catchError(error => {
          console.error('Ошибка при создании животного:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Животное успешно создано" });
          this.getAnimals();
          this.newAnimal = {};
          this.showCreateModal = false;
        })
      )
      .subscribe();
  }

  updateAnimal(id: string, animal: Animal) {
    this.loading = true;
    this.http.put(`https://api.snglrtycrvtureofspce.me/Animal/${id}`, animal)
      .pipe(
        catchError(error => {
          console.error('Ошибка при обновлении животного:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Животное успешно обновлено" });
          this.getAnimals();
          this.editingAnimal = null;
        })
      )
      .subscribe();
  }

  deleteAnimal(id: string) {
    this.loading = true;
    this.http.delete(`https://api.snglrtycrvtureofspce.me/Animal/${id}`)
      .pipe(
        catchError(error => {
          console.error('Ошибка при удалении животного:', error);
          return EMPTY;
        }),
        finalize(() => {
          this.toast.success({ detail: "Успех", summary: "Животное успешно удалено" });
          this.getAnimals();
        })
      )
      .subscribe();
  }

  openEditModal(animal: Animal) {
    this.editingAnimal = { ...animal };
  }

  cancelEdit() {
    this.editingAnimal = null;
  }

  onFileChange(event: Event, mode: 'create' | 'edit') {
    const input = event.target as HTMLInputElement;
    if (input.files && input.files[0]) {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        if (mode === 'create') {
          this.newAnimal.photoUrl = e.target.result;
        } else {
          this.editingAnimal!.photoUrl = e.target.result;
        }
      };
      reader.readAsDataURL(input.files[0]);
    }
  }

  FilterFn() {
    const AnimalNameFilter = this.AnimalNameFilter.toLowerCase().trim();
    const AnimalDescriptionFilter = this.AnimalDescriptionFilter.toLowerCase().trim();
    const AnimalSquadFilter = this.AnimalSquadFilter.toLowerCase().trim();
    const AnimalFamilyFilter = this.AnimalFamilyFilter.toLowerCase().trim();
    const AnimalRodFilter = this.AnimalRodFilter.toLowerCase().trim();
    const AnimalViewFilter = this.AnimalViewFilter.toLowerCase().trim();
    const AnimalInternationalScientificNameFilter = this.AnimalInternationalScientificNameFilter.toLowerCase().trim();
    const AnimalEncyclopediaUrlFilter = this.AnimalEncyclopediaUrlFilter.toLowerCase().trim();
    const AnimalPopulationFilter = this.AnimalPopulationFilter.toLowerCase().trim();
    const AnimalPhotoUrlFilter = this.AnimalPhotoUrlFilter.toLowerCase().trim();
    const AnimalClassNameFilter = this.AnimalClassNameFilter.toLowerCase().trim();

    this.animals = this.animalsWithoutFilter.filter(el => {
      const className = this.getAnimalClassName(el.animalClassId).toLowerCase();
      return (
        el.name.toLowerCase().includes(AnimalNameFilter) &&
        el.description.toLowerCase().includes(AnimalDescriptionFilter) &&
        el.squad.toLowerCase().includes(AnimalSquadFilter) &&
        el.family.toLowerCase().includes(AnimalFamilyFilter) &&
        el.rod.toLowerCase().includes(AnimalRodFilter) &&
        el.view.toLowerCase().includes(AnimalViewFilter) &&
        el.internationalScientificName.toLowerCase().includes(AnimalInternationalScientificNameFilter) &&
        el.encyclopediaUrl.toLowerCase().includes(AnimalEncyclopediaUrlFilter) &&
        el.population.toString().toLowerCase().includes(AnimalPopulationFilter) &&
        el.photoUrl.toString().toLowerCase().includes(AnimalPhotoUrlFilter) &&
        className.includes(AnimalClassNameFilter)
      );
    });
  }

  sortResult(prop: keyof Animal, asc: boolean) {
    this.sortColumn = prop;
    this.sortAsc = asc;
    this.animals.sort((a, b) => {
      if (asc) {
        return a[prop] > b[prop] ? 1 : a[prop] < b[prop] ? -1 : 0;
      } else {
        return b[prop] > a[prop] ? 1 : b[prop] < a[prop] ? -1 : 0;
      }
    });
  }
}