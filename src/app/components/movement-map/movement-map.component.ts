import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';
import { EMPTY } from 'rxjs';

declare var google: any;

interface MovementPoint {
  id: string;
  latitude: number;
  longitude: number;
  animalId: string;
  locationId: string;
  verified: boolean;
}

interface Animal {
  id: string;
  createdDate: string;
  modificationDate: string;
  name: string;
  description: string;
  squad: string;
  family: string;
  rod: string;
  view: string;
  internationalScientificName: string;
  encyclopediaUrl: string;
  population: number;
  photoUrl: string;
  animalClassId: string;
}

interface Location {
  id: string;
  createdDate: string;
  modificationDate: string;
  name: string;
  description: string;
  latitude: number;
  longitude: number;
}

@Component({
  selector: 'app-movement-map',
  templateUrl: './movement-map.component.html',
  styleUrls: ['./movement-map.component.scss']
})
export class MovementMapComponent implements OnInit {
  map: any;
  movementPoints: MovementPoint[] = [];
  animals: Animal[] = [];
  locations: Location[] = [];
  loading = false;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.getAnimals();
    this.getLocations();
  }

  initMap(): void {
    const position = { lat: 52.5692827, lng: 23.8029082 };
    this.map = new google.maps.Map(document.getElementById('map') as HTMLElement, {
      center: position,
      zoom: 8
    });
  }

  getMovementPoints(): void {
    this.loading = true;
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: MovementPoint[] }>('https://api.snglrtycrvtureofspce.me/MovementPoint')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка точек перемещения:', error);
          return EMPTY;
        }),
        finalize(() => this.loading = false)
      )
      .subscribe(response => {
        this.movementPoints = response.elements;
        this.initMap();
        this.plotMarkers();
      });
  }

  getAnimals() {
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: Animal[] }>('https://api.snglrtycrvtureofspce.me/Animal')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка животных:', error);
          return EMPTY;
        })
      )
      .subscribe(response => {
        this.animals = response.elements;
        this.getMovementPoints();
      });
  }

  getLocations() {
    this.http.get<{ message: string, statusCode: number, page: number, count: number, total: number, elements: Location[] }>('https://api.snglrtycrvtureofspce.me/Location')
      .pipe(
        catchError(error => {
          console.error('Ошибка при получении списка местоположений:', error);
          return EMPTY;
        })
      )
      .subscribe(response => {
        this.locations = response.elements;
      });
  }

  plotMarkers(): void {
    this.movementPoints.forEach(point => {
      const animal = this.animals.find(a => a.id === point.animalId);
      const location = this.locations.find(a => a.id === point.locationId);
      if (animal) {
        const marker = new google.maps.Marker({
          position: { lat: point.latitude, lng: point.longitude },
          map: this.map,
          title: animal.name
        });

        const infowindow = new google.maps.InfoWindow({
          content: `<div><strong>${animal.name}, ${location?.name}</strong></div>`
        });

        marker.addListener('click', () => {
          infowindow.open(this.map, marker);
        });
      }
    });
  }
}