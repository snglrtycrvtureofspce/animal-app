import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import ValidateForm from '../../helpers/validationform';
import { Router } from '@angular/router';
import { countries } from './../../models/countries';
import { cities } from './../../models/cities';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  public signUpForm!: FormGroup;
  type: string = 'password';
  isText: boolean = false;
  eyeIcon: string = "fa-eye-slash";
  countries: string[] = countries;
  cities: { [key: string]: string[] } = cities;

  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router, private toast: NgToastService) { }

  ngOnInit() {
    this.signUpForm = this.fb.group({
      firstName: ['', [Validators.required, Validators.pattern(/^[A-Za-zА-Яа-я]+$/)]],
      lastName: ['', [Validators.required, Validators.pattern(/^[A-Za-zА-Яа-я]+$/)]],
      middleName: [''],
      userName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/)]],
      passwordConfirm: ['', Validators.required],
      dateOfBirth: ['', [Validators.required, this.dateOfBirthValidator]],
      country: [''],
      city: [''],
      agreement: [false, Validators.requiredTrue]
    }, {
      validators: this.passwordMatchValidator
    });
  }

  passwordMatchValidator(form: FormGroup) {
    return form.get('password')!.value === form.get('passwordConfirm')!.value ? null : { mismatch: true };
  }

  dateOfBirthValidator(control: FormGroup): { [key: string]: boolean } | null {
    const dateValue = control.value;
    if (!dateValue) {
      return { 'invalidDate': true };
    }

    const currentDate = new Date();
    const inputDate = new Date(dateValue);

    if (isNaN(inputDate.getTime()) || inputDate >= currentDate) {
      return { 'invalidDate': true };
    }
    return null;
  }

  hideShowPass() {
    this.isText = !this.isText;
    this.isText ? this.eyeIcon = 'fa-eye' : this.eyeIcon = 'fa-eye-slash';
    this.isText ? this.type = 'text' : this.type = 'password';
  }

  onSubmit() {
    if (this.signUpForm.valid) {
      const dob = this.signUpForm.value.dateOfBirth;
      const formattedDOB = dob ? new Date(dob).toISOString() : null;
      let signUpObj = {
        ...this.signUpForm.value,
        dateOfBirth: formattedDOB
      };
      this.auth.signUp(signUpObj)
        .subscribe({
          next: (res => {
            console.log(res.message);
            this.signUpForm.reset();
            this.router.navigate(['login']);
            this.toast.success({ detail: "Успех", summary: "Регистрация успешна, пожалуйста авторизируйтесь!" });
          }),
          error: (err) => {
            this.toast.error({ detail: "Ошибка", summary: "Что-то пошло не так!", duration: 5000 });
            console.log(err);
          },
        });
    } else {
      ValidateForm.validateAllFormFields(this.signUpForm);
    }
  }
}