import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt'
import { TokenApiModel } from '../models/token-api.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private baseUrl: string = 'https://api.snglrtycrvtureofspce.me/User/';
  private userPayload: any;

  constructor(private http: HttpClient, private router: Router) {
    this.userPayload = this.decodedToken();
  }

  getUserIdByAccessToken(accessTokenObj: string): Observable<string> {
    return this.http.post<any>(`${this.baseUrl}GetUserIdByAccessToken`, accessTokenObj);
  }

  signUp(userObj: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}RegisterUser`, userObj);
  }

  signIn(loginObj: any): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}AuthenticateUser`, loginObj);
  }

  signOut() {
    localStorage.clear();
    this.router.navigate(['login'])
  }

  storeUserId(userIdValue: string) {
    localStorage.setItem('userId', userIdValue)
  }

  storeAccessToken(tokenValue: string) {
    localStorage.setItem('accessToken', tokenValue)
  }

  storeRefreshToken(tokenValue: string) {
    localStorage.setItem('refreshToken', tokenValue)
  }

  getAccessToken() {
    return localStorage.getItem('accessToken')
  }

  getRefreshToken() {
    return localStorage.getItem('refreshToken')
  }

  getUserId() {
    return localStorage.getItem('userId')
  }

  getUserRole(): string | null {
    const decodedToken = this.decodedToken();
    return decodedToken ? decodedToken.role : null;
  }

  getUserEmail(): string | null {
    const decodedToken = this.decodedToken();
    return decodedToken.email;
  }

  isLoggedIn(): boolean {
    return !!localStorage.getItem('accessToken')
  }

  decodedToken() {
    const jwtHelper = new JwtHelperService();
    const accessToken = this.getAccessToken()!;
    return jwtHelper.decodeToken(accessToken)
  }

  isAdmin(): boolean {
    const decodedToken = this.decodedToken();
    return decodedToken && decodedToken.Role === 'Administrator';
  }

  isUser(): boolean {
    const decodedToken = this.decodedToken();
    return decodedToken && decodedToken.Role === 'Member';
  }

  getfullNameFromToken() {
    if (this.userPayload)
      return this.userPayload.name;
  }

  getRoleFromToken() {
    if (this.userPayload)
      return this.userPayload.role;
  }

  renewToken(tokenApi: TokenApiModel): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}RefreshToken`, tokenApi);
  }
}
