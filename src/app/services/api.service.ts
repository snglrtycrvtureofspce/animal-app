import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  private baseUrl: string = 'https://api.snglrtycrvtureofspce.me/User/';
  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get<any>(this.baseUrl).pipe(
      map((response: any) => response.users)
    );
  }
}
