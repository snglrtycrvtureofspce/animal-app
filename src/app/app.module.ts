import { SignupComponent } from './components/signup/signup.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgToastModule } from 'ng-angular-popup';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { NewsComponent } from './components/news/news.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LicenseComponent } from './components/license/license.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsOfServiceComponent } from './components/terms-of-service/terms-of-service.component';
import { AccountComponent } from './components/account/account.component';
import { AnimalClassComponent } from './components/animal-class/animal-class.component';
import { AnimalComponent } from './components/animal/animal.component';
import { LocationComponent } from './components/location/location.component';
import { MovementPointComponent } from './components/movement-point/movement-point.component';
import { MovementMapComponent } from './components/movement-map/movement-map.component';
import { AnimalReportComponent } from './components/animal-report/animal-report.component';
import { ChatComponent } from './components/chat/chat.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    NewsComponent,
    HeaderComponent,
    FooterComponent,
    LicenseComponent,
    PrivacyPolicyComponent,
    TermsOfServiceComponent,
    AccountComponent,
    AnimalClassComponent,
    AnimalComponent,
    LocationComponent,
    MovementPointComponent,
    MovementMapComponent,
    AnimalReportComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    NgToastModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule { }
