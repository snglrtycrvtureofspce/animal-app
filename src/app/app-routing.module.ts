import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SignupComponent } from './components/signup/signup.component';
import { LoginComponent } from './components/login/login.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MultiGuard } from './guards/multi.guard';
import { AdminGuard } from './guards/admin.guard';
import { UserGuard } from './guards/user.guard';
import { AuthGuard } from './guards/auth.guard';
import { NewsComponent } from './components/news/news.component';
import { LicenseComponent } from './components/license/license.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { TermsOfServiceComponent } from './components/terms-of-service/terms-of-service.component';
import { AccountComponent } from './components/account/account.component';
import { AnimalClassComponent } from './components/animal-class/animal-class.component';
import { AnimalComponent } from './components/animal/animal.component';
import { LocationComponent } from './components/location/location.component';
import { MovementPointComponent } from './components/movement-point/movement-point.component';
import { MovementMapComponent } from './components/movement-map/movement-map.component';
import { HomeComponent } from './components/home/home.component';
import { AnimalReportComponent } from './components/animal-report/animal-report.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent, canActivate: [AuthGuard] },
  { path: 'signup', component: SignupComponent, canActivate: [AuthGuard] },
  { path: 'account', component: AccountComponent, canActivate: [MultiGuard] },
  { path: 'news', component: NewsComponent, canActivate: [MultiGuard] },
  { path: 'animal-class', component: AnimalClassComponent, canActivate: [MultiGuard] },
  { path: 'animal', component: AnimalComponent, canActivate: [MultiGuard] },
  { path: 'location', component: LocationComponent, canActivate: [MultiGuard] },
  { path: 'movement-point', component: MovementPointComponent, canActivate: [MultiGuard] },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AdminGuard] },
  { path: 'movement-map', component: MovementMapComponent, canActivate: [MultiGuard] },
  { path: 'animal-report', component: AnimalReportComponent, canActivate: [AdminGuard] },
  { path: 'license', component: LicenseComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: 'terms-of-service', component: TermsOfServiceComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
