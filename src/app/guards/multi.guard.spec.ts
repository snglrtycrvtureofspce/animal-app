import { TestBed } from '@angular/core/testing';

import { MultiGuard } from './multi.guard';

describe('MultiGuard', () => {
  let guard: MultiGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(MultiGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
