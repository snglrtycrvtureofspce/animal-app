import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';

@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router, private toast: NgToastService) { }

  canActivate(): boolean {
    console.log(this.auth.isUser);
    if (this.auth.isUser()) {
      return true;
    } else {
      this.toast.error({ detail: "Ошибка", summary: "Пожалуйста авторизируйтесь!" });
      this.router.navigate(['login']);
      return false;
    }
  }
}
