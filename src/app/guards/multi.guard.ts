import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { NgToastService } from 'ng-angular-popup';

@Injectable({
  providedIn: 'root'
})
export class MultiGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router, private toast: NgToastService) { }

  canActivate(): boolean {
    if (this.auth.isAdmin()) {
      return true;
    } else if (this.auth.isUser()) {
      return true;
    } else {
      this.toast.error({
        detail: "Ошибка",
        summary: this.auth.isLoggedIn() ? "У вас нет прав доступа." : "Пожалуйста авторизируйтесь!"
      });
      this.router.navigate(['login']);
      return false;
    }
  }
}