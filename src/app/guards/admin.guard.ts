import { AuthService } from '../services/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router, private toast: NgToastService) { }

  canActivate(): boolean {
    if (this.auth.isAdmin()) {
      return true;
    } else {
      this.toast.error({ detail: "Ошибка", summary: "У вас нет прав доступа, свяжитесь с администратором" });
      this.router.navigate(['home']);
      return false;
    }
  }
}